import json
import requests

data = {}
for i in range(22, 26):
    res = requests.get(f"https://rickandmortyapi.com/api/character/?page={i}")
    for person in list(res.json()['results']):         
        data[person['id']] = {person['name']: { person['location']['url'] : person['location']['name']}}

json_string = json.dumps(data)
with open('json_data.json', 'w') as outfile:
    json.dump(json_string, outfile)
