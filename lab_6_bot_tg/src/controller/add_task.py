from todoist import add_task
from telebot import telebot, types


def read_task(chat_id, bot, message):
    sent = bot.send_message(chat_id, "Введите ткст новой задачи:")
    bot.register_next_step_handler(sent, get_text_message_ans_add_task, bot, chat_id)

def get_text_message_ans_add_task(sent, bot, chat_id):
    add_task(sent.text)
    bot.send_message(chat_id, "Задача " + sent.text + " добавлена")