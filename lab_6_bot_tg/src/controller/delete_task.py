from todoist import get_name_tasks
from todoist import delete_task_for_id
from todoist import get_tasks

from telebot import telebot, types

def get_list_task(chat_id, bot):
    markup = telebot.types.InlineKeyboardMarkup()
    i=0
    for task in get_tasks():
        markup.add(telebot.types.InlineKeyboardButton(text=task.content, callback_data=task.id))
        i += 1
    bot.send_message(chat_id, text="Какую задачу хотите удалить?", reply_markup=markup)

def dlelete_task(bot, call):
    name_task = get_name_tasks(call.data)
    delete_task_for_id(call.data)
    print("delete ", call.data)
    bot.send_message(call.message.chat.id, "Задача " + name_task + " удалено")
    bot.edit_message_reply_markup(call.message.chat.id, call.message.message_id)