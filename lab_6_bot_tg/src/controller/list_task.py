# import todoist
from telebot import telebot, types

from todoist import get_tasks

def get_str_tasks(chat_id, bot):
    i=0
    tasks = ""
    for task in get_tasks():
        tasks += (str(i) + ". "+ task.content + "\n")
        i += 1
    bot.send_message(chat_id, text=tasks)



