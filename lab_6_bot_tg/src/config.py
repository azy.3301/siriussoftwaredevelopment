import os

from dotenv import load_dotenv

load_dotenv()

TODOIST_API = os.environ.get('TODOIST_API')


BOT_TOKEN = os.environ.get('BOT_TOKEN')
