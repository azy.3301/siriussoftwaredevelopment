from config import TODOIST_API
from todoist_api_python.api import TodoistAPI
import json
import requests


TODOIST_API = TodoistAPI(TODOIST_API)


def add_task(text):
    if is_labels("Basket"):
        print("True")
        try:
            task = TODOIST_API.add_task(
                content=text, 
                labels=["Basket"],
                # due_string="tomorrow at 12:00",
                due_lang="en",
                priority=1,
            )
        except Exception as error:
            print(error)
    else:
        print("False")
        label = TODOIST_API.add_label(name="Basket")
        print(label)
        try:
            task = TODOIST_API.add_task(
                content=text, 
                labels=["Basket"],
                # due_string="tomorrow at 12:00",
                due_lang="en",
                priority=1,
            )
        except Exception as error:
            print(error)


def get_name_tasks(id_task):
    try:
        task = TODOIST_API.get_task(task_id=id_task)
        return(task.content)
    except Exception as error:
        print(error)


def get_tasks():
    try:
        tasks = TODOIST_API.get_tasks()
        return tasks
    except Exception as error:
        print(error)


def is_labels(name):
    try:
        labels = TODOIST_API.get_labels()
        for label in labels:
            if label.name == name:
                return True
        return False                         
    except Exception as error:
        print(error)



def task_id_for_name(name_task):
    try:
        tasks = TODOIST_API.get_tasks()
        for task in tasks:
            if task.content == name_task:
                return task.id
    except Exception as error:
        print(error)



def delete_task_for_name(name_task):
    try:
        is_success = TODOIST_API.delete_task(task_id=task_id_for_name(name_task))
        print(is_success)
    except Exception as error:
        print(error)
    
def delete_task_for_id(id_task):
    try:
        is_success = TODOIST_API.delete_task(task_id=id_task)
        print(is_success)
    except Exception as error:
        print(error)
    



def close_task_for_id(id_task):
    try:
        is_success = TODOIST_API.close_task(task_id=id_task)
        print(is_success)
    except Exception as error:
        print(error)

def close_task_for_name(name_task):
    try:
        is_success = TODOIST_API.close_task(task_id=task_id_for_name(name_task))
        print(is_success)
    except Exception as error:
        print(error)


# add_task("test_4")

# delete_task("test_3")
# delete_task("test_4")

# close_task("test_4")