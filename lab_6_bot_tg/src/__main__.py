from config import BOT_TOKEN
from telebot import telebot, types

from controller.delete_task import get_list_task
from controller.add_task import read_task
from controller.delete_task import dlelete_task
from controller.list_task import get_str_tasks
from controller.close_task import get_list_open_task



print(BOT_TOKEN)
bot = telebot.TeleBot(BOT_TOKEN)


@bot.message_handler(commands=['start'])
def welcome(message):
 
    # keyboard
    markup = types.ReplyKeyboardMarkup(resize_keyboard=True)
    item1 = types.KeyboardButton("Добавить  задачу")
    item2 = types.KeyboardButton("Закрыть задачу")
    item3 = types.KeyboardButton("Удалить задачу")
    item4 = types.KeyboardButton("Посомотреть списалк задач из inbox")

 
    markup.add(item1, item2, item3,item4)

    bot.send_message(message.chat.id, "Добро пожаловать, {0.first_name}!\nЯ - <b>{1.first_name}</b>,бот который поможет отслеживать и добавлять свои задачи прямо в todoist.".format(message.from_user, bot.get_me()),
        parse_mode='html', reply_markup=markup)

@bot.message_handler(content_types=['text'])
def message_processing(message):
     if message.chat.type == 'private':
        if message.text == 'Добавить  задачу':
            read_task(message.chat.id, bot, message)
        elif message.text == "Закрыть задачу":
            get_list_open_task(message.chat.id, bot)
        elif message.text == "Удалить задачу":
            get_list_task(message.chat.id, bot)
        elif message.text == "Посомотреть списалк задач из inbox":
            get_str_tasks(message.chat.id, bot)
        else:
            bot.send_message(message.chat.id, 'Tакой команды нет')
 

@bot.callback_query_handler(func=lambda call: True)
def query_handler(call):
    dlelete_task(bot, call)

if __name__ == '__main__':
  bot.polling(none_stop=True)