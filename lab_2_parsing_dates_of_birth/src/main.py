import requests
import statistics
import datetime

TOKEN = "vk1.a.CA2mbb_C5fAnkQdN7UqRjfxgxrKOjCvMG4F_LO04FqOV0ukgSRm-7CgEV5mib4Q9kPFedaJxFSMtwexptz3Pkv15tDOfvswgPh7KKSKAsCsWY3WIfP6d2zPya7l3JlWmDJYtvtIwEy3IFrRd_pPvH0eMDowhuX0D8vWY6Ec31be71i5likgHhGNXU2tp_wn_&expires_in=0"

def get_average_age ():
    url = f'https://api.vk.com/method/friends.get?fields=bdate&access_token={TOKEN}&v=5.131'
    response = requests.get(url)
    data = response.json()
    list_year = []

    for item in data['response']['items']:
        try:
            data = (item["bdate"])
            
            if len(data) > 2:
                hash = int(data[-4:])
                list_year.append(hash)
                average = round(statistics.mean(list_year))
        except: pass
    return datetime.datetime.today().year - average
            
if __name__ == '__main__':
    average = get_average_age()
    print("Средний возраст равен: ",  average)
    
